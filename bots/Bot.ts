import { BotFactory } from '../src/Types.d.ts'

const bot: BotFactory = () => {
	const WIN = 1
	const TIE = 0
	const LOSS = -1

	let moves =
	{
		'paper': {sampleSize: 0, wins: 0, ties: 0, losses: 0, score: 0.0},
		'rock': {sampleSize: 0, wins: 0, ties: 0, losses: 0, score: 0.0},
		'scissors': {sampleSize: 0, wins: 0, ties: 0, losses: 0, score: 0.0},
	}

	// Called after each round with the results
	// result is 1 for win, -1 for loss, 0 for tie
	const Report = (myMove: string, theirMove: string, result: int) => 
	{
		// Update win rates
		moves[myMove]['sampleSize']++
		switch (result) 
		{
			case WIN:
				moves[myMove]['wins']++
				break;

			case TIE:
				moves[myMove]['ties']++
				break;

			case LOSS:
				moves[myMove]['losses']++
				break;
		}

		moves[myMove]['score'] = (moves[myMove]['wins'] - moves[myMove]['losses']) / moves[myMove]['sampleSize']
	}

	// Decide what to do
	const Shoot = () => 
	{
		let bestMove = 'paper'
		let leastSampledMove = 'paper'
		
		// Find highest win rate move
		for (let move in moves) 
		{
			if (moves[move]['sampleSize'] < moves[leastSampledMove]['sampleSize'])
			{
				leastSampledMove = move
			}

			if (moves[move]['score'] > moves[bestMove]['score'])
			{
				bestMove = move
			}
		}

		// Make sure to sample every move sufficiently
		if (3*moves[leastSampledMove]['sampleSize'] < (moves[bestMove]['sampleSize']))
		{
			bestMove = leastSampledMove
		}

		return bestMove
	}

	return {
		Name: 'Frug',
		Shoot,
		Report,
	}
}

export default bot
