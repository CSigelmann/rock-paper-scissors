import { Timeout } from 'https://deno.land/x/timeout@2.4/mod.ts'
import { Bot, BotFactory, Move } from './Types.d.ts'

function isValid(move: Move) {
	return move === 'rock' || move === 'paper' || move === 'scissors'
}

function beats(attack: Move, defense: Move): boolean {
	return (attack === 'rock' && defense === 'scissors') ||
		(attack === 'scissors' && defense === 'paper') ||
		(attack === 'paper' && defense === 'rock')
}

function reportDisqualification(botName: string, shot: Move) {
	console.log(botName, 'played an invalid move and is disqualified.', shot)
}

function formatBold(str: string) {
	return '\x1b[1m' + str + '\x1b[0m'
}

function formatWinner(str: string) {
	return '\x1b[32m' + str + '\x1b[0m'
}

export async function battle(botA: Bot, botB: Bot, numWins = 2) {
	const maxTies = 20
	let ties = 0
	let aWins = 0
	let bWins = 0

	console.log()
	console.log(formatBold(botA.Name), 'vs', formatBold(botB.Name))
	await Timeout.wait(1000)

	while (aWins < numWins && bWins < numWins && ties < maxTies) {
		await Timeout.wait(500)

		const aShot = botA.Shoot()
		const bShot = botB.Shoot()
		let result = 0

		if (!isValid(aShot)) {
			reportDisqualification(botA.Name, aShot)
			return 1
		}

		if (!isValid(bShot)) {
			reportDisqualification(botB.Name, bShot)
			return -1
		}

		if (beats(aShot, bShot)) {
			aWins++
			ties = 0
			result = 1
			console.log(formatWinner(aShot), bShot)
		} else if (beats(bShot, aShot)) {
			bWins++
			ties = 0
			result = -1
			console.log(aShot, formatWinner(bShot))
		} else {
			console.log(aShot, bShot)
			result = 0
			ties++
		}

		botA.Report(aShot, bShot, result)
		botB.Report(bShot, aShot, -1 * result)
	}

	console.log(aWins, ' - ', bWins)

	if (aWins > bWins) {
		console.log(formatWinner(botA.Name), 'beats', botB.Name)
		return 1
	}

	if (bWins > aWins) {
		console.log(formatWinner(botB.Name), 'beats', botA.Name)
		return -1
	}

	console.log('Stalemate')
	return 0
}

export async function roundRobin(bots: Array<BotFactory>, winsPerMatch = 2) {
	let scores = bots.map(() => 0)

	// Play games
	for (let i = 0; i < bots.length - 1; i++) {
		for (let j = i + 1; j < bots.length; j++) {
			const result = await battle(bots[i](), bots[j](), winsPerMatch)
			await Timeout.wait(2000)

			if (result > 0) {
				scores[i]++
			} else if (result < 0) {
				scores[j]++
			}
		}
	}

	// Find winner
	let max = -1
	let maxBot = ''

	console.log()
	console.log('Final Standings:')

	for (let i = 0; i < bots.length; i++) {
		const name = bots[i]().Name

		console.log(name, scores[i])

		if (scores[i] > max) {
			max = scores[i]
			maxBot = name
		}
	}

	console.log()
	console.log('Our tournament winner is', formatWinner(maxBot))
}
